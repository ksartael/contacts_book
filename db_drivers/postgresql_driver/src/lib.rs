//! Copyright © 2024 Александр Кубраков
//!
//! Licensed under the Apache License, Version 2.0 (the "License");
//! you may not use this file except in compliance with the License.
//! You may obtain a copy of the License at
//!
//! <https://www.apache.org/licenses/LICENSE-2.0>
//!
//! Unless required by applicable law or agreed to in writing, software
//! distributed under the License is distributed on an "AS IS" BASIS,
//! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//! See the License for the specific language governing permissions and
//! limitations under the License.
pub use deadpool_postgres::tokio_postgres::NoTls;
use deadpool_postgres::Client;
pub use deadpool_postgres::{Config, Pool, PoolError, Runtime};
use postgres_from_row::FromRow;
use serde::{Deserialize, Serialize};
pub type ID = i64;
#[derive(FromRow, Serialize, Deserialize)]
pub struct Contact {
    id: Option<ID>,
    first_name: String,
    surname: String,
    telephone: String,
}
impl Contact {
    pub fn new(first_name: String, surname: String, telephone: String) -> Contact {
        Contact {
            id: None,
            first_name,
            surname,
            telephone,
        }
    }
    fn get_first_name(&self) -> &String {
        &self.first_name
    }
    fn get_surname(&self) -> &String {
        &self.surname
    }
    fn get_telephone(&self) -> &String {
        &self.telephone
    }
}

pub async fn add_contacts(client: &Client, contacts: Vec<Contact>) -> Result<u64, PoolError> {
    let stmt = include_str!("../sql/add_contacts.sql");
    let stmt = client.prepare(stmt).await?;
    let mut count: u64 = 0;
    for contact in contacts {
        count += client
            .execute(
                &stmt,
                &[
                    contact.get_first_name(),
                    contact.get_surname(),
                    contact.get_telephone(),
                ],
            )
            .await?;
    }
    Ok(count)
}
pub async fn get_contacts(client: &Client) -> Result<Vec<Contact>, PoolError> {
    let stmt = include_str!("../sql/get_contacts.sql");
    let stmt = client.prepare(stmt).await?;
    let result: Vec<Contact> = client
        .query(&stmt, &[])
        .await?
        .iter()
        .map(Contact::from_row)
        .collect();
    Ok(result)
}
pub async fn remove_contacts(client: &Client, vec_id: Vec<ID>) -> Result<u64, PoolError> {
    let stmt = include_str!("../sql/remove_contacts.sql");
    let stmt = client.prepare(stmt).await?;
    let mut count: u64 = 0;
    for id in vec_id {
        count += client.execute(&stmt, &[&id]).await?;
    }
    Ok(count)
}
