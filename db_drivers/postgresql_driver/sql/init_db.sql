DROP SCHEMA IF EXISTS test_contacts_book CASCADE;
CREATE SCHEMA test_contacts_book;
CREATE TABLE test_contacts_book.contacts (
	id  BIGSERIAL PRIMARY KEY,
	first_name VARCHAR(200) NOT NULL,
	surname VARCHAR(200) NOT NULL,
	telephone VARCHAR(50) UNIQUE NOT NULL
);
GRANT ALL PRIVILEGES ON SCHEMA test_contacts_book TO test_user;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA test_contacts_book TO test_user;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA test_contacts_book TO test_user;
