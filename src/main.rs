//! Copyright © 2024 Александр Кубраков
//!
//! Licensed under the Apache License, Version 2.0 (the "License");
//! you may not use this file except in compliance with the License.
//! You may obtain a copy of the License at
//!
//! <https://www.apache.org/licenses/LICENSE-2.0>
//!
//! Unless required by applicable law or agreed to in writing, software
//! distributed under the License is distributed on an "AS IS" BASIS,
//! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//! See the License for the specific language governing permissions and
//! limitations under the License.
use actix_web::{middleware, web, App, HttpServer};
use contacts_book::config::ServerConfig;
use contacts_book::handlers::{add_contacts, get_contacts, remove_contacts};
use contacts_book::log::setup_logging;
#[actix_web::main]
async fn main() -> std::io::Result<()> {
    setup_logging(0).expect("Failed to initialize logging!");

    let config = ServerConfig::from_env().expect("Reading config file .env failed!");

    let pool = config.create_pool();

    let server = HttpServer::new(move || {
        App::new()
            .wrap(middleware::Logger::default())
            .app_data(web::Data::new(pool.clone()))
            .service(
                web::scope("/contacts")
                    .route("/all", web::get().to(get_contacts))
                    .route("/add", web::post().to(add_contacts))
                    .route("/remove", web::post().to(remove_contacts)),
            )
    })
    .bind(config.get_server_addr())?
    .run();
    println!("Server running at http://{}/", config.get_server_addr());

    server.await
}
