#![doc = include_str!("../README.md")]
//!
//! Copyright © 2024 Александр Кубраков
//!
//! Licensed under the Apache License, Version 2.0 (the "License");
//! you may not use this file except in compliance with the License.
//! You may obtain a copy of the License at
//!
//! <https://www.apache.org/licenses/LICENSE-2.0>
//!
//! Unless required by applicable law or agreed to in writing, software
//! distributed under the License is distributed on an "AS IS" BASIS,
//! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//! See the License for the specific language governing permissions and
//! limitations under the License.
use postgresql_driver as db_driver;
pub mod config {
    use crate::db_driver;
    use config;
    use dotenvy::dotenv;
    use serde::Deserialize;
    #[derive(Debug, Default, Deserialize)]
    pub struct ServerConfig {
        server_addr: String,
        db: db_driver::Config,
    }
    impl ServerConfig {
        pub fn from_env() -> Result<Self, config::ConfigError> {
            dotenv().ok();
            config::Config::builder()
                .add_source(config::Environment::default())
                .build()?
                .try_deserialize()
        }
        pub fn create_pool(&self) -> db_driver::Pool {
            self.db
                .create_pool(Some(db_driver::Runtime::Tokio1), db_driver::NoTls)
                .expect("Creation pool failed!")
        }
        pub fn get_server_addr(&self) -> &String {
            &self.server_addr
        }
    }
}
pub mod log {
    use fern;
    use std::{io, time::SystemTime};
    pub fn setup_logging(verbosity: u64) -> Result<(), fern::InitError> {
        let mut base_config = fern::Dispatch::new();

        base_config = match verbosity {
            0 => base_config
                .level(log::LevelFilter::Info)
                .level_for("overly-verbose-target", log::LevelFilter::Warn),
            1 => base_config
                .level(log::LevelFilter::Debug)
                .level_for("overly-verbose-target", log::LevelFilter::Info),
            2 => base_config.level(log::LevelFilter::Debug),
            _3_or_more => base_config.level(log::LevelFilter::Trace),
        };

        let file_config = fern::Dispatch::new()
            .format(|out, message, record| {
                out.finish(format_args!(
                    "[{} {} {}] {}",
                    humantime::format_rfc3339_seconds(SystemTime::now()),
                    record.level(),
                    record.target(),
                    message
                ))
            })
            .chain(fern::log_file("server.log")?);

        let stdout_config = fern::Dispatch::new()
            .format(|out, message, record| {
                out.finish(format_args!(
                    "[{} {} {}] {}",
                    humantime::format_rfc3339_seconds(SystemTime::now()),
                    record.level(),
                    record.target(),
                    message
                ))
            })
            .chain(io::stdout());

        base_config
            .chain(file_config)
            .chain(stdout_config)
            .apply()?;

        Ok(())
    }
}

mod errors {
    use crate::db_driver::PoolError;
    use actix_web::{HttpResponse, ResponseError};
    use derive_more::{Display, From};
    #[derive(Display, From, Debug)]
    pub enum ServerError {
        NotFound,
        PoolError(PoolError),
    }
    impl std::error::Error for ServerError {}

    impl ResponseError for ServerError {
        fn error_response(&self) -> HttpResponse {
            match *self {
                ServerError::NotFound => HttpResponse::NotFound().finish(),
                ServerError::PoolError(ref err) => {
                    HttpResponse::InternalServerError().body(err.to_string())
                }
            }
        }
    }
}

pub mod handlers {
    use crate::db_driver;
    use crate::errors::ServerError;
    use actix_web::{web, HttpResponse};
    /// Получение всех полей всех контактов из БД
    pub async fn get_contacts(
        db_pool: web::Data<db_driver::Pool>,
    ) -> Result<HttpResponse, ServerError> {
        let client = db_pool.get().await?;

        let contacts = db_driver::get_contacts(&client).await?;

        Ok(HttpResponse::Ok().json(contacts))
    }
    /// Добавление новых контактов в БД
    pub async fn add_contacts(
        contacts_info: web::Json<Vec<db_driver::Contact>>,
        db_pool: web::Data<db_driver::Pool>,
    ) -> Result<HttpResponse, ServerError> {
        let contacts: Vec<db_driver::Contact> = contacts_info.into_inner();

        let client = db_pool.get().await?;

        let count = db_driver::add_contacts(&client, contacts).await?;

        Ok(HttpResponse::Ok().json(count))
    }
    /// Удаление контактов из БД по их id
    pub async fn remove_contacts(
        contacts_info: web::Json<Vec<db_driver::ID>>,
        db_pool: web::Data<db_driver::Pool>,
    ) -> Result<HttpResponse, ServerError> {
        let vec_id: Vec<db_driver::ID> = contacts_info.into_inner();

        let client = db_pool.get().await?;

        let count = db_driver::remove_contacts(&client, vec_id).await?;

        Ok(HttpResponse::Ok().json(count))
    }
}
